$(window).scroll(onScroll);

function onScroll(e) {
    const className = 'btn-scroll-top';
    if (window.scrollY > window.innerHeight) {
        if (!$(`.${className}`).length) {
            $('<button>Наверх</button>')
                .css({
                    position: 'fixed',
                    right: 10,
                    bottom: 10
                })
                .addClass(className)
                .click(scrollTop)
                .appendTo(document.body);
        }
    } else {
        $(`.${className}`).remove();
    }
}
function scrollTop() {
    $("html, body").animate({scrollTop: 0}, 1000);
}

$('.popular-posts span').click(function () {
    let showHide = $('.popular-posts span');
    $('.posts').fadeToggle(500);
});
